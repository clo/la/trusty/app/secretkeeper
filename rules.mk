# Copyright (C) 2022 The Android Open Source Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

LOCAL_DIR := $(GET_LOCAL_DIR)

MODULE := $(LOCAL_DIR)

MANIFEST := $(LOCAL_DIR)/manifest.json

MODULE_SRCS += \
	$(LOCAL_DIR)/lib.rs \

MODULE_CRATE_NAME := secretkeeper

MODULE_LIBRARY_DEPS += \
	trusty/user/base/lib/authgraph-rust/boringssl \
	trusty/user/base/lib/authgraph-rust/core \
	trusty/user/base/lib/authgraph-rust/wire \
	trusty/user/base/lib/secretkeeper/comm \
	trusty/user/base/lib/secretkeeper/core \
	trusty/user/base/lib/secretkeeper/dice_policy \
	$(call FIND_CRATE,coset) \
	$(call FIND_CRATE,log) \
	trusty/user/base/lib/storage/rust \
	trusty/user/base/lib/tipc/rust \
	trusty/user/base/lib/trusty-log \
	trusty/user/base/lib/trusty-std \

MODULE_RUST_TESTS := true

# The port tests are built and installed regardless of whether the Secretkeeper TA
# is enabled, so set a config value to allow tests that involve the TA to be skipped.
ifeq (true,$(call TOBOOL,$(SECRETKEEPER_ENABLED)))
     MODULE_RUSTFLAGS += --cfg 'secretkeeper_enabled'
endif

MODULE_RUST_USE_CLIPPY := true

include make/library.mk
